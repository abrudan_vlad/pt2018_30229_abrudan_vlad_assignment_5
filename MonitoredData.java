import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import static java.time.temporal.ChronoUnit.SECONDS;


public class MonitoredData  {
private LocalDateTime startTime;
private LocalDateTime endTime;
private  String activity;

public MonitoredData(String s) {
	activity=s.split("\t")[4];
	startTime=LocalDateTime.parse(s.split("\t")[0]+" "+s.split("\t")[1], DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss "));
	endTime=LocalDateTime.parse(s.split("\t")[2]+" "+s.split("\t")[3], DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss "));

}

public static List<MonitoredData> readActivityes() throws IOException{
	return Files.lines(Paths.get("C:\\Users\\user\\Desktop\\WorkSpaceEclipse\\Tema5 TP\\src\\Activity.txt"))
			.map(x->{
				return new MonitoredData(x);
				}).collect(Collectors.toList());
}
//1
public static Long countingDays(List<MonitoredData> lista) {
	List<Integer> endTimeList=lista.stream().map(x->x.getEndTime().getDayOfYear()).distinct().collect(Collectors.toList());
	List<Integer> startTimeList=lista.stream().map(x->x.getStartTime().getDayOfYear()).distinct().collect(Collectors.toList());
	endTimeList.addAll(startTimeList);
	return endTimeList.stream().distinct().count();
}
//2
public static Map<String, Long> activityCounter(List<MonitoredData>list ){
	Map<String, Long> map=list.stream().collect(Collectors.groupingBy(x->x.getActivity(),Collectors.counting()));		
	return map;
}
//3
public static Map<Integer, Map<String, Long>> activityCounterForDays(List<MonitoredData> list) throws IOException{
	return list.stream().collect(Collectors.groupingBy(x->x.getStartTime().getDayOfYear(),Collectors.groupingBy(x->x.getActivity(),Collectors.counting())));	
}
//4
public static Map<String,Long> eachDuration(List<MonitoredData> lista){
	Map<String, Long> list= lista.stream().collect(Collectors.groupingBy(x->x.getActivity(), Collectors.summingLong(x->{return Duration.between(x.getStartTime(), x.getEndTime()).get(SECONDS);})));
	return 	list.entrySet().stream().filter(x->x.getValue()/3600>=10).filter(x->x.setValue(x.getValue()/3600)>=10).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
}
//5
public static List<String> activityFilter(List<MonitoredData> lista){
	Map<String, Long> map=lista.stream().collect(Collectors.groupingBy(x->x.getActivity(),Collectors.counting()));
	Map<String, Long>map2=lista.stream().filter(x->Duration.between(x.getStartTime(),x.getEndTime()).getSeconds()<=300).collect(Collectors.groupingBy(x->x.getActivity(),Collectors.counting()));
	Set<String> set=map.keySet();
	for (String string : set) {
		map2.putIfAbsent(string, (long) 0);
	}
	return lista.stream().map(x->x.getActivity()).filter(x->map2.get(x)/map.get(x)>=0.9).distinct().collect(Collectors.toList());
}

public static void writeMap(Map<String,Long> map, int fileNumber) {
	Formatter x=null;
	try {
		x=new Formatter("Data"+fileNumber+".txt");
		for (String activity :map.keySet()) {
			x.format("%s:",activity);
			x.format("%d\n",map.get(activity));
		}
		x.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}

public static void writeMap2(Map<Integer,Map<String, Long>> map, int fileNumber) {
	Formatter x=null;
	try {
		x=new Formatter("Data"+fileNumber+".txt");
		for (Integer key :map.keySet()) {
			x.format("Ziua:%s activitati:%s\n",key,map.get(key));
		}
		x.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}

public static void writeMap3(List<String> lista, int fileNumber) {
	Formatter x=null;
	try {
		x=new Formatter("Data"+fileNumber+".txt");
		for (String string :lista) {
			x.format("%s\n",string);
		}
		x.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}

public String getActivity() {
	return activity;
}

public LocalDateTime getEndTime() {
	return endTime;
}

public LocalDateTime getStartTime() {
	return startTime;
}

public String toString() {
	return startTime+","+activity+","+endTime;
}

public static void main(String[] args) {
	List<MonitoredData> lista=new ArrayList<MonitoredData>();
	try {
		lista=readActivityes();
	}
	catch (IOException e) {}
	
	for (MonitoredData monitoredData : lista) {
		System.out.println(monitoredData);
	}
	
	try {
		writeMap(activityCounter(lista),1);
		writeMap(eachDuration(lista), 2);
		writeMap2(activityCounterForDays(lista), 3);
		writeMap3(activityFilter(lista), 4);
	} 
	catch (IOException e) {
		e.printStackTrace();}
	
}


}
